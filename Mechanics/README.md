# Mechanics

## Description
\*.ipynb, \*.md and \*.png files with math formulas and graphs describing the rules and physics of the game.