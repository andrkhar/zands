# Vision

## Description
There are general schemes and descriptions of story, interface, gameplay and other information - the core ideas what the game is planned to be.
Each idea is represented by \*.md or \*.png file.

## Purpose
The folder serves **Single Source Of Truth** about the project to keep decisions consistent.