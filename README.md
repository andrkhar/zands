# Zands
## Description:
3d low poly submarine simulation game (with VR support)

## Licence:
Open Software License 3.0

## Platform:
Android 9.0+

## Frontend:
Unreal Engine 4.23

## Backend
Django 2.2